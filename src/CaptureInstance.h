/*
 * CaptureInstance.h
 *
 *  Created on: Apr 25, 2012
 *      Author: tmh
 */

#ifndef CAPTUREINSTANCE_H_
#define CAPTUREINSTANCE_H_

#include <pcap.h>

struct in6_addr;
struct ether_header;
class IPCache;
class Config;

class CaptureInstance
{
protected:
	const char *m_interface;
	pcap_t *m_handle;
	bpf_program m_fp;
	IPCache *m_cache;
	Config *m_config;
	in6_addr m_network;
	int m_netmask;

	int open();
	int close();
	int pcap_error(const char *err);

	static void _callback(u_char *args, const struct pcap_pkthdr *header, const u_char *packet);
	void callback(const struct pcap_pkthdr *header, const u_char *packet);
        void dispatch(ether_header *header, u_int8_t *mac, const in6_addr *addr);

	int classify_packet(const in6_addr *addr);
	int network_and_netmask(const char *value, in6_addr *network, int *netmask);
	int compare_address(const in6_addr *addr1, const in6_addr *addr2, int netmask);

public:
	CaptureInstance(const char *interface, IPCache *cache, Config *config);
	virtual ~CaptureInstance();

	int run();
	void stop();
};

#endif /* CAPTUREINSTANCE_H_ */
