/*
 * NewIpThread.h
 *
 *  Created on: Apr 29, 2012
 *      Author: tmh
 */

#ifndef NEWIPTHREAD_H_
#define NEWIPTHREAD_H_

#include <netinet/in.h>
#include <pthread.h>

class Config;

class NewIpThread
{
private:
	u_int8_t m_mac[6];
	sockaddr_in6 m_addr;
	pthread_t m_thread;
	Config *m_config;

	static void *_thread_start(void *arg);
	void *thread_start();

	void parse_autoreverse(const char *hostname, char *line);
	void register_rdns(const char *hostname);

public:
	NewIpThread(const u_int8_t *mac, const in6_addr *addr, Config *config);
	virtual ~NewIpThread();

	int run();
	void register_new_ip();
};

#endif /* NEWIPTHREAD_H_ */
