#include <stdio.h>
#include <string.h>
#include <arpa/inet.h>

const char *mac_to_string(char *buf, const u_int8_t *mac, char separator)
{
        if(!separator)
                snprintf(buf, 20, "%02x%02x%02x%02x%02x%02x",
                        mac[0],
                        mac[1],
                        mac[2],
                        mac[3],
                        mac[4],
                        mac[5]);
        else
                snprintf(buf, 20, "%02x%c%02x%c%02x%c%02x%c%02x%c%02x",
                        mac[0], separator,
                        mac[1], separator,
                        mac[2], separator,
                        mac[3], separator,
                        mac[4], separator,
                        mac[5]);
        return buf;
}

const char *ip6_to_string(char *buf, const in6_addr *addr)
{
        return inet_ntop(AF_INET6, addr, buf, INET6_ADDRSTRLEN);
}

const char *rdns_to_string(char *buf, const in6_addr *addr)
{
        char *p = buf;
        for(int n=15; n>=0; --n)
        {
                sprintf(p, "%x.%x.", addr->s6_addr[n] & 0xf, addr->s6_addr[n] >> 4);
                p += 4;
        }
        strcpy(p,"ip6.arpa");
}

void generate_eui64(const u_int8_t *mac, const in6_addr *base_addr, in6_addr *addr)
{
        memcpy(addr->s6_addr, base_addr->s6_addr, 8);
        addr->s6_addr[8] = mac[0] ^ 0x02;
        addr->s6_addr[9] = mac[1];
        addr->s6_addr[10] = mac[2];
        addr->s6_addr[11] = 0xff;
        addr->s6_addr[12] = 0xfe;
        addr->s6_addr[13] = mac[3];
        addr->s6_addr[14] = mac[4];
        addr->s6_addr[15] = mac[5];
}
