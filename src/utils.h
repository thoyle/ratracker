#ifndef UTILS__H
#define UTILS__H

const char *mac_to_string(char *buf, const u_int8_t *mac, char separator);
const char *ip6_to_string(char *buf, const in6_addr *addr);
const char *rdns_to_string(char *buf, const in6_addr *addr);
void generate_eui64(const u_int8_t *mac, const in6_addr *base_addr, in6_addr *addr);

#endif

