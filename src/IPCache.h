/*
 * IPCache.h
 *
 *  Created on: Apr 26, 2012
 *      Author: tmh
 */

#ifndef IPCACHE_H_
#define IPCACHE_H_

#include <map>
#include <netinet/in.h>

class IP6Addr
{
private:
	in6_addr m_addr;

public:
	IP6Addr(const in6_addr *addr);
	virtual ~IP6Addr();

	const in6_addr *addr() const;
	bool operator==(const IP6Addr& other) const;
	bool operator==(const in6_addr *other) const;
	bool operator<(const IP6Addr& other) const;
};

struct CacheData
{
	u_int8_t mac[6];
	time_t lastseen;
};

class IPCache
{
protected:
	typedef std::map<IP6Addr, CacheData> cache_t;
	cache_t m_cache;

public:
	IPCache();
	virtual ~IPCache();

	bool exists(const in6_addr *addr);
	void add(const u_int8_t *mac, const in6_addr *addr);
	void dump_cache(const char *filename);
};


#endif /* IPCACHE_H_ */
