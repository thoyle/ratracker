/*
 * NewIpThread.cpp
 *
 *  Created on: Apr 29, 2012
 *      Author: tmh
 */
#include <netinet/in.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <syslog.h>

#include "Config.h"
#include "NewIpThread.h"
#include "utils.h"

static int debug;

NewIpThread::NewIpThread(const u_int8_t *mac, const in6_addr *addr, Config *config)
{
	debug = config->intValue("debug", 0);

	memcpy(m_mac, mac, 6);
	memset(&m_addr, 0, sizeof(m_addr));
	m_addr.sin6_family = AF_INET6;
	memcpy(&m_addr.sin6_addr, addr, sizeof(m_addr.sin6_addr));
	m_config = config;
}

NewIpThread::~NewIpThread()
{
}

void *NewIpThread::_thread_start(void *arg)
{
	return ((NewIpThread*)arg)->thread_start();
}

void *NewIpThread::thread_start()
{
	register_new_ip();
	delete this; // When we die, the object dies too
	return NULL;
}

int NewIpThread::run()
{
	return pthread_create(&m_thread, NULL, _thread_start, this);
}

void NewIpThread::register_new_ip()
{
	sockaddr_in6 eui64 = { AF_INET6 };
	char hostname[NI_MAXHOST];
	char macbuf[24], ip6buf[48];
	char newrev[256];
	const char *autorev;

	if(debug)
		printf("New IP from %s   %s\n", mac_to_string(macbuf, m_mac, ':'), ip6_to_string(ip6buf, &m_addr.sin6_addr));

	if(getnameinfo((sockaddr*)&m_addr, sizeof(m_addr), hostname, sizeof(hostname), NULL, 0, NI_NAMEREQD))
	{
		if(debug)
			printf("Does not exist in RDNS\n");
		generate_eui64(m_mac, &m_addr.sin6_addr, &eui64.sin6_addr);
		printf("EUI-64 is %s\n", ip6_to_string(ip6buf, &eui64.sin6_addr));
		if(getnameinfo((sockaddr*)&eui64, sizeof(eui64), hostname, sizeof(hostname), NULL, 0, NI_NAMEREQD))
		{
			if(debug)
				printf("EUI-64 does not exist in RDNS\n");
			autorev = m_config->stringValue("autoreverse", "");
			if(*autorev)
			{
				parse_autoreverse(autorev, newrev);
				if(debug)
					printf("Generating auto-reverse: %s\n",newrev);
				syslog(LOG_INFO, "New host on %s: Registering RDNS as %s", mac_to_string(macbuf, m_mac, ':'), newrev);
				register_rdns(newrev);
			}
		}
		else
		{
			if(debug)
				printf("EUI-64 exists in RDNS as %s\n", hostname);
			syslog(LOG_INFO, "New host on %s: Registering RDNS as %s", mac_to_string(macbuf, m_mac, ':'), hostname);
			register_rdns(hostname);
		}
	}
	else
		if(debug)
			printf("Already exists in RDNS as %s\n", hostname);
}

void NewIpThread::parse_autoreverse(const char *hostname, char *line)
{
	const char *p;
	char *q;

	for(p=hostname, q=line; *p; p++)
	{
		if(*p=='%' and *(p+1)=='e')
		{
			mac_to_string(q, m_mac, 0);
			q += strlen(q);
			p++;	
		}
		else
			*(q++)=*p;
	}
	*(q++)='\0';
}

void NewIpThread::register_rdns(const char *hostname)
{
	const char *p;
	char *q;
	char line[1024];
	const char *cmd = m_config->stringValue("update_cmd","");

	if(!*cmd)
	{
		if(debug)
			printf("Cannot update - No update command set\n");
		return;
	}

	for(p=cmd, q=line; *p; p++)
	{
		if(*p=='%' and *(p+1)=='h')
		{
			strcpy(q, hostname);
			q += strlen(q);
			p++;	
		}
		else if(*p=='%' and *(p+1)=='a')
		{
			ip6_to_string(q, &m_addr.sin6_addr);
			q += strlen(q);
			p++;	
		}
		else if(*p=='%' and *(p+1)=='r')
		{
			rdns_to_string(q, &m_addr.sin6_addr);
			q += strlen(q);
			p++;	
		}
		else if(*p=='%' and *(p+1)=='e')
		{
			mac_to_string(q, m_mac, ':');
			q += strlen(q);
			p++;	
		}
		else
			*(q++)=*p;
	}
	*(q++)='\0';

	if(debug)
		printf("Executing %s\n", line);
	system(line);
}
