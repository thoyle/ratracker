/*
 * ratracker.cpp
 *
 *  Created on: Apr 25, 2012
 *      Author: tmh
 */
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <syslog.h>
#include <signal.h>

#include "IPCache.h"
#include "CaptureInstance.h"
#include "Config.h"

IPCache *g_cache;
CaptureInstance *g_capture;

extern "C" void dump_config(int signal)
{
	g_cache->dump_cache("/tmp/ratracker.cache");
	if(signal == SIGTERM)
		g_capture->stop();
}

extern "C" int main(int argc, char *argv[])
{
	if(argc != 1)
	{
		fprintf(stderr,"Usage: %s\n", argv[0]);
		return EXIT_FAILURE;
	}
	Config *config = new Config("ratracker.cfg");
	if(!config->read())
	{
		fprintf(stderr,"Couldn't read config file\n");
		return EXIT_FAILURE;
	}

	if(config->intValue("debug",0)==0)
		daemon(0,0);
	else
		printf("In debug mode: not backgrounding");

	openlog("ratracker", LOG_PID, LOG_DAEMON);

	signal(SIGHUP, dump_config);
	signal(SIGTERM, dump_config);

	const char *interface = config->stringValue("interface", "eth0");

	g_cache = new IPCache;
	g_capture = new CaptureInstance(interface, g_cache, config);
	g_capture->run();

	signal(SIGHUP, SIG_DFL);
	signal(SIGTERM, SIG_DFL);

	delete g_capture;
	delete g_cache;

	closelog();

	return EXIT_SUCCESS;
}
