/*
 * CaptureInstance.cpp
 *
 *  Created on: Apr 25, 2012
 *      Author: tmh
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <net/ethernet.h>
#include <netinet/in.h>
#include <netinet/ip6.h>
#include <arpa/inet.h>

#include "CaptureInstance.h"
#include "IPCache.h"
#include "NewIpThread.h"
#include "Config.h"

#define FILTER_EXP "ip6"

#define PKT_LINKLOCAL 0x0001
#define PKT_EUI64     0x0002
#define PKT_MULTICAST 0x0004

CaptureInstance::CaptureInstance(const char *interface, IPCache *cache, Config *config)
{
	m_interface = interface;
	m_handle = NULL;
	m_cache = cache;
	m_config = config;
}

CaptureInstance::~CaptureInstance()
{
	close();
}

int CaptureInstance::open()
{
	char errbuf[PCAP_ERRBUF_SIZE];

	close();

	if(network_and_netmask(m_config->stringValue("network", "::/0"), &m_network, &m_netmask))
	{
		fprintf(stderr, "Couldn't parse network filter: %s\n",m_config->stringValue("network", "::/0"));
		return -1;
	}

	m_handle = pcap_open_live(m_interface, 128, m_config->intValue("promisc", 1), 1000, errbuf);
	if(!m_handle)
	{
		fprintf(stderr, "Couldn't open device %s: %s\n", m_interface, errbuf);
		return -1;
	}

	if(pcap_compile(m_handle, &m_fp, FILTER_EXP, 0, PCAP_NETMASK_UNKNOWN) == -1)
		return pcap_error("Couldn't compile expression");

	if(pcap_setfilter(m_handle, &m_fp) == -1)
		return pcap_error("Couldn't set filter");

	return 0;
}

int CaptureInstance::close()
{
	if(m_handle)
		pcap_close(m_handle);
	m_handle = NULL;
	return 0;
}

int CaptureInstance::run()
{
	int ret;
	pcap_pkthdr header;
	const u_char *packet;

	if((ret=open())!=0)
		return ret;

	if(pcap_loop(m_handle, -1, _callback, (u_char*)this) == -1)
		return pcap_error("Capture error");

	close();

	return 0;
}

void CaptureInstance::stop()
{
	pcap_breakloop(m_handle);
}

int CaptureInstance::pcap_error(const char *err)
{
	fprintf(stderr, "%s: %s\n",err, pcap_geterr(m_handle));
	return 1;
}

void CaptureInstance::_callback(u_char *args, const struct pcap_pkthdr *header, const u_char *packet)
{
	((CaptureInstance *)args)->callback(header, packet);
}

void CaptureInstance::callback(const struct pcap_pkthdr *header, const u_char *packet)
{
	ether_header *ether;
	ip6_hdr *ip6;

	if(header->len < sizeof(ether_header) + sizeof(ip6_hdr))
		return;

	ether = (ether_header*)packet;
	ip6 = (ip6_hdr *)(packet + sizeof(ether_header));

	if(ntohs(ether->ether_type) != ETHERTYPE_IPV6)
		return; /* Can't happen, unless libpcap is bust */

	dispatch(ether, ether->ether_shost, &ip6->ip6_src);
	dispatch(ether, ether->ether_dhost, &ip6->ip6_dst);
}

void CaptureInstance::dispatch(ether_header *header, u_int8_t *mac, const in6_addr *addr)
{
	int packet_class;
	packet_class = classify_packet(addr);

	if((packet_class & (PKT_LINKLOCAL|PKT_MULTICAST)) == 0)
	{
		if(compare_address(&m_network, addr, m_netmask))
			return;

		if(!m_cache->exists(addr))
		{
			m_cache->add(mac, addr);
			NewIpThread *thread = new NewIpThread(mac, addr, m_config);
			thread->run();
			// Thread will delete itself
		}
	}
}

int CaptureInstance::classify_packet(const in6_addr *addr)
{
	int res = 0;

	if(addr->s6_addr[0] == 0xFE && addr->s6_addr[1] == 0x80)
		res |= PKT_LINKLOCAL;

	if(addr->s6_addr[11] == 0xFF && addr->s6_addr[12] == 0xFE)
		res |= PKT_EUI64;

	if(addr->s6_addr[0] == 0xFF)
		res |= PKT_MULTICAST;

	return res;
}

int CaptureInstance::network_and_netmask(const char *value, in6_addr *network, int *netmask)
{
	int nm;
	char *v = strdup(value);
	char *p = strchr(v,'/');

	if(!p || !*p)
		*netmask = 128;
	else
	{
		*p='\0';
		nm = atoi(p+1);
		if(nm<0 || nm>128)
		{
			free(v);
			return -1;
		}
		*netmask = nm;
	}


	if(inet_pton(AF_INET6, v, network)==0)
	{
		free(v);
		return -1;
	}

	free(v);
	return 0;
}

int CaptureInstance::compare_address(const in6_addr *addr1, const in6_addr *addr2, int netmask)
{
	int bytes = netmask >> 3;
	int bits = netmask & 7;
	int res;

	if(bytes == 0) return 0;
	if(bytes > 16) return -1;  // Shouldn't happen

	res = memcmp(addr1->s6_addr, addr2->s6_addr, bytes);
	if(res == 0 && bytes < 16 && bits > 0)
	{
		int mask = (0xff00 >> bits) & 0xff;
		res = int(addr1->s6_addr[bytes] & mask) - int(addr2->s6_addr[bytes] & mask);
	}
	return res;
}
