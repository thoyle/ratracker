LDFLAGS = -g -lpcap -lpthread
CFLAGS = -g 
VPATH=src

all: ratracker

clean:
	rm -f ratracker *.o

ratracker: ratracker.cpp utils.cpp CaptureInstance.cpp IPCache.cpp NewIPThread.cpp Config.cpp
