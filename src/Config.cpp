/*
 * Config.cpp
 *
 *  Created on: Apr 29, 2012
 *      Author: tmh
 */
#include <stdio.h>
#include <stdlib.h>

#include "Config.h"

Config::Config(const char *filename)
{
	m_filename = filename;
}

Config::~Config()
{
}

bool Config::read()
{
	char *line = NULL;
	char *p,*q;
	const char *key, *value;
	size_t linecap = 0;
	ssize_t linelen;

	FILE *f = fopen(m_filename, "r");
	if(f == NULL)
		return false;

	while((linelen = getline(&line, &linecap, f)) > 0)
	{
		for(p = line; *p && isspace(*p); p++)
			;
		if(!*p)
			continue;

		if(*p == ';')
			continue;

		key = p;

		for(;*p && (*p)!='='; p++)
			;

		if(!*p)
			continue;

		q=p-1;
		for(q--; isspace(*q) && q>=key; q--)
			;
		*(++q) = '\0';
		if(q==key)
			continue;

		for(p++; *p && isspace(*p); p++)
			;

		if(!*p)
			continue;

		value = p;

		if(*p == '\'' || *p == '"')
		{
			for(p++; *p && (*p)!=(*value); p++)
				;
			*p='\0';
			value++;
		}
		else
		{
			for(; *p && !isspace(*p); p++)
				;
			*p='\0';
		}

		m_config[key]=value;
	}

	fclose(f);
	return true;
}

bool Config::boolValue(const char *value, bool defaultValue)
{
	stringMap_t::const_iterator i = m_config.find(value);
	if(i != m_config.end())
	{
		const char *v = i->second.c_str();
		if(v[0]=='y' || v[0]=='Y' || v[0]=='t' || v[0]=='T' || atoi(v)!=0) return true;
		if(v[0]=='n' || v[0]=='N' || v[0]=='f' || v[0]=='f' || atoi(v)==0) return false;
		return defaultValue;
	}
	else
		return defaultValue;
}

int Config::intValue(const char *value, int defaultValue)
{
	stringMap_t::const_iterator i = m_config.find(value);
	if(i != m_config.end())
		return atoi(i->second.c_str());
	else
		return defaultValue;
}

const char *Config::stringValue(const char *value, const char *defaultValue)
{
	stringMap_t::const_iterator i = m_config.find(value);
	if(i != m_config.end())
		return i->second.c_str();
	else
		return defaultValue;
}
