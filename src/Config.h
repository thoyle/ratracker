/*
 * Config.h
 *
 *  Created on: Apr 29, 2012
 *      Author: tmh
 */

#ifndef CONFIG_H_
#define CONFIG_H_

#include <map>
#include <string>

class Config
{
	typedef std::map<std::string, std::string> stringMap_t;
private:
	const char *m_filename;
	stringMap_t m_config;

public:
	Config(const char *file);
	~Config();

	bool read();
	bool boolValue(const char *value, bool defaultValue);
	int intValue(const char *value, int defaultValue);
	const char *stringValue(const char *value, const char *defaultValue);
};

#endif /* CONFIG_H_ */
