/*
 * IPCache.cpp
 *
 *  Created on: Apr 26, 2012
 *      Author: tmh
 */

#include <netinet/in.h>
#include <time.h>
#include <string.h>
#include <stdio.h>

#include "IPCache.h"
#include "utils.h"

/* IPCache */

IPCache::IPCache()
{
}

IPCache::~IPCache()
{
}

bool IPCache::exists(const in6_addr *addr)
{
	return m_cache.find(addr) != m_cache.end();
}

void IPCache::add(const u_int8_t *mac, const in6_addr *addr)
{
	memcpy(m_cache[addr].mac, mac, 6);
	m_cache[addr].lastseen = time(NULL);
}

void IPCache::dump_cache(const char *filename)
{
	char tmp[128];

	FILE *f=fopen(filename,"w");
	for(cache_t::const_iterator i = m_cache.begin(); i!=m_cache.end(); ++i)
	{
		fprintf(f," %s {\n", ip6_to_string(tmp, i->first.addr()));
		fprintf(f,"  mac=%s\n", mac_to_string(tmp, i->second.mac, ':'));	
		fprintf(f,"  lastseen=%lu\n", i->second.lastseen);	
		fprintf(f,"}\n");
	}
	fclose(f);
}

/* IP6Addr */

IP6Addr::IP6Addr(const in6_addr *addr)
{
	memcpy(&m_addr, addr, sizeof(in6_addr));
}

IP6Addr::~IP6Addr()
{
}

const in6_addr *IP6Addr::addr() const
{
	return &m_addr;
}

bool IP6Addr::operator==(const in6_addr *other) const
{
	return !memcmp(&m_addr, other, sizeof(in6_addr));
}

bool IP6Addr::operator==(const IP6Addr& other) const
{
	return !memcmp(&m_addr, &other.m_addr, sizeof(in6_addr));
}

bool IP6Addr::operator<(const IP6Addr& other) const
{
	return memcmp(&m_addr, &other.m_addr, sizeof(in6_addr)) < 0;
}
